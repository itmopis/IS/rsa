from decimal import Decimal
from typing import TextIO


def get_input(*ins: TextIO) -> (list[int], list[TextIO]):
    N = []
    C = []
    for ini in ins:
        N.append(int(ini.readline()))
        ini.readline()
        C.append(ini)
    return N, C


def solve(in1: str, in2: str, in3: str, trace: bool = False) -> str:
    ans = ""
    with open(in1) as in1:
        with open(in2) as in2:
            with open(in3) as in3:
                (N1, N2, N3), C = get_input(in1, in2, in3)

                M0 = N1 * N2 * N3

                m1 = N2 * N3
                m2 = N1 * N3
                m3 = N1 * N2
                n1 = pow(m1, -1, mod=N1)
                n2 = pow(m2, -1, mod=N2)
                n3 = pow(m3, -1, mod=N3)

                if trace:
                    print(f"M₀ = N₁∙N₂∙N₃ = {N1}∙{N2}∙{N3} = {M0}", "\n")
                    print(f"m₁ = N₂∙N₃ = {N2}∙{N3} = {m1}")
                    print(f"m₂ = N₁∙N₃ = {N1}∙{N3} = {m2}")
                    print(f"m₃ = N₁∙N₂ = {N1}∙{N2} = {m3}", "\n")
                    print(f"n₁ = m₁^(-1) mod N₁ = {m1}^(-1) mod {N1} = {n1}")
                    print(f"n₂ = m₂^(-1) mod N₂ = {m2}^(-1) mod {N2} = {n2}")
                    print(f"n₃ = m₃^(-1) mod N₃ = {m3}^(-1) mod {N3} = {n3}", "\n")

                while C[0].readable:
                    try:
                        c1 = int(C[0].readline())
                        c2 = int(C[1].readline())
                        c3 = int(C[2].readline())
                    except ValueError:
                        break

                    S = (c1 * n1 * m1) + (c2 * n2 * m2) + (c3 * n3 * m3)

                    M = round((S % M0) ** (Decimal(1 / 3)))
                    part = M.to_bytes(4, byteorder='big').decode('cp1251')
                    ans += part
                    if trace:
                        print(f"S = c₁∙n₁∙m₁ + c₂∙n₂∙m₂ + c₃∙n₃∙m₃ = {S}")
                        print(f"M = (S mod M₀)^(1/3) = ({S % M0})^(1/3) = {M}")
                        print(f"text(M) = {part}", "\n")
    return ans

def main():
    print(f"Итоговая расшифровка: '{solve("1.in", "2.in", "3.in", True)}'")


if __name__ == '__main__':
    main()
